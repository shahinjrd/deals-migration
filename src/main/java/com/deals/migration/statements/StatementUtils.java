/**
 * @File: StatementUtils.java
 */
package com.deals.migration.statements;

import java.util.Map;
import java.util.Set;

import com.deals.migration.logger.DmLogger;

/**
 * @author shahinkonadath
 * @date 25-Apr-2015
 * @package com.deals.migration.statements
 * @project deals-migration
 * @desc
 */
public class StatementUtils {

	private static DmLogger dmLogger = new DmLogger(StatementUtils.class);

	/**
	 * @param unit
	 * @return
	 */
	public static String getSql(Map<String, Integer> columnMap, String tableName) {
		Set<String> keySet = columnMap.keySet();
		String[] columns = new String[columnMap.size()];

		for (String key : keySet) {
			columns[columnMap.get(key) - 1] = key;
		}

		return getSqlString(columns, tableName);
	}

	/**
	 * @param columnList
	 * @return
	 */
	private static String getSqlString(String[] columnArray, String tableName) {

		StringBuilder sqlSb = new StringBuilder();

		sqlSb.append("INSERT INTO ");
		sqlSb.append(tableName + " (");

		StringBuilder columnSb = new StringBuilder(0);
		StringBuilder valueSb = new StringBuilder(0);
		for (String column : columnArray) {
			if (columnSb.length() != 0) {
				columnSb.append(", ");
			}
			columnSb.append(column);

			if (valueSb.length() != 0) {
				valueSb.append(", ");
			}
			valueSb.append("?");
		}

		sqlSb.append(columnSb.toString() + ") ");
		sqlSb.append("VALUES (");
		sqlSb.append(valueSb.toString() + ")");
		System.out.println("SQL: " + sqlSb);
		dmLogger.debug("SQL: " + sqlSb);
		return sqlSb.toString();
	}

}// End of the class