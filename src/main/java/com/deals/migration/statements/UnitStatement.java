/**
 * @File: UnitStatement.java
 */
package com.deals.migration.statements;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.deals.migration.domain.Unit;

/**
 * @author shahinkonadath
 * @date 25-Apr-2015
 * @package com.deals.migration.statements
 * @project deals-migration
 * @desc
 */
public class UnitStatement implements GenericStatementSetter<Unit> {

	@Override
	public void setValues(Unit unit, PreparedStatement ps) throws SQLException {

		Map<String, Integer> columnMap = getColumnMap();
		ps.setLong(columnMap.get("client_id"), unit.getClientId());
		ps.setLong(columnMap.get("user_id"), unit.getUserId());
		ps.setString(columnMap.get("ref_code"), unit.getRefCode());
		ps.setString(columnMap.get("unit_number"), unit.getUnitNumber());
		ps.setString(columnMap.get("street_number"), unit.getStreetNumber());
		ps.setLong(columnMap.get("category_id"), unit.getCategoryId());
		ps.setString(columnMap.get("beds"), unit.getBeds());
		ps.setString(columnMap.get("baths"), unit.getBaths());
		ps.setFloat(columnMap.get("built_up_area"), unit.getBuiltUpArea());
		ps.setString(columnMap.get("plot"), unit.getPlot());
		ps.setDouble(columnMap.get("guide_price"), unit.getGuidePrice());
		ps.setDouble(columnMap.get("fees"), unit.getFees());
		ps.setString(columnMap.get("title"), unit.getTitle());
		ps.setString(columnMap.get("description"), unit.getDescription());
		ps.setString(columnMap.get("furnished"), unit.getFurnished());
		ps.setDate(columnMap.get("date_added"), new java.sql.Date(unit.getDateAdded().getTime()));
		ps.setDate(columnMap.get("date_updated"), new java.sql.Date(unit.getDateUpdated().getTime()));
		ps.setLong(columnMap.get("landlord_id"), unit.getLandlordId());
		ps.setLong(columnMap.get("region_id"), unit.getRegionId());
		ps.setLong(columnMap.get("location_id"), unit.getLocationId());
		ps.setLong(columnMap.get("sub_location_id"), unit.getSubLocationId());
		ps.setString(columnMap.get("view"), unit.getView());
		ps.setString(columnMap.get("photos"), unit.getPhotos());
		ps.setString(columnMap.get("floor"), unit.getFloor());
		ps.setString(columnMap.get("type"), unit.getType());
		ps.setBlob(columnMap.get("image_store"), new javax.sql.rowset.serial.SerialBlob(((null == unit.getImages() || unit
				.getImages().equals("")) ? "Empty" : unit.getImages()).getBytes()));
		ps.setString(columnMap.get("fitted"), unit.getFitted());
		ps.setBoolean(columnMap.get("is_active"), unit.isIsActive());

	}

	@Override
	public Map<String, Integer> getColumnMap() {

		Map<String, Integer> columnMap = new HashMap<String, Integer>();
		columnMap.put("client_id", 1);
		columnMap.put("user_id", 2);
		columnMap.put("ref_code", 3);
		columnMap.put("unit_number", 4);
		columnMap.put("street_number", 5);
		columnMap.put("category_id", 6);
		columnMap.put("beds", 7);
		columnMap.put("baths", 8);
		columnMap.put("built_up_area", 9);
		columnMap.put("plot", 10);
		columnMap.put("guide_price", 11);
		columnMap.put("fees", 12);
		columnMap.put("title", 13);
		columnMap.put("description", 14);
		columnMap.put("furnished", 15);
		columnMap.put("date_added", 16);
		columnMap.put("date_updated", 17);		
		columnMap.put("landlord_id", 18);
		columnMap.put("region_id", 19);
		columnMap.put("location_id", 20);
		columnMap.put("sub_location_id", 21);
		columnMap.put("view", 22);
		columnMap.put("photos", 23);
		columnMap.put("floor", 24);
		columnMap.put("type", 25);
		columnMap.put("image_store", 26);
		columnMap.put("fitted", 27);
		columnMap.put("is_active", 28);
		//columnMap.put("country_id", 18);
		return columnMap;
	}

	@Override
	public String getSql() {
		return StatementUtils.getSql(getColumnMap(), "cms_pm_unit");
	}

}
