/**
 * @File: PmContactStatement.java
 */
package com.deals.migration.statements;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.deals.migration.domain.PmContact;

/**
 * @author shahinkonadath
 * @date 25-Apr-2015
 * @package com.deals.migration.statements
 * @project deals-migration
 * @desc
 */
public class PmContactStatement implements GenericStatementSetter<PmContact> {

	@Override
	public void setValues(PmContact item, PreparedStatement ps) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public Map<String, Integer> getColumnMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSql() {
		// TODO Auto-generated method stub
		return null;
	}

}// End of the class