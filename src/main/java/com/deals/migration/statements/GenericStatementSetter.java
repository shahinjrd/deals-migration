/**
 * @File: GenericStatementSetter.java
 */
package com.deals.migration.statements;

import java.util.Map;

import org.springframework.batch.item.database.ItemPreparedStatementSetter;

/**
 * @author shahinkonadath
 * @date 25-Apr-2015
 * @package com.deals.migration.statements
 * @project deals-migration
 * @desc 
 */
public interface GenericStatementSetter<T> extends ItemPreparedStatementSetter<T>{
	
	/**
	 * Map holding the column names and its corresponding indexes.
	 * @return
	 */
	public Map<String, Integer> getColumnMap();
	
	/**
	 * Method to generate the insert/update statements
	 * @return
	 */
	public String getSql();

}
