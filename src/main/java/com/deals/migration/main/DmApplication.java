/**
 * @File: DmApplication.java
 */
package com.deals.migration.main;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.deals.migration.config.DealsMigrationConfig;

/**
 * @author shahinkonadath
 * @date 22-Apr-2015
 * @package com.deals.migration.main
 * @project deals-migration
 * @desc
 */
@SpringBootApplication
@Import(DealsMigrationConfig.class)
public class DmApplication implements CommandLineRunner {

	@Autowired
	private JobLauncher jobLauncher;

	@Autowired
	private Job job;

	public static void main(String[] args) {
		SpringApplication.run(DmApplication.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		jobLauncher.run(job, new JobParameters());
	}

}// End of the class