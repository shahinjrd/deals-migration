/**
 * @File: PsImagesMapper.java
 */
package com.deals.migration.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.deals.migration.domain.PsImage;

/**
 * @author shahinkonadath
 * @date 23-Apr-2015
 * @package com.deals.migration.mapper
 * @project deals-migration
 * @desc 
 */
public class PsImagesMapper implements RowMapper<PsImage>{

	
	@Override
	public PsImage mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		PsImage psImage = new PsImage();		
		psImage.setId(rs.getLong("id"));
		psImage.setRandKey(rs.getLong("rand_key"));
		psImage.setClientId(rs.getLong("clientId"));
		psImage.setUserId(rs.getLong("user_id"));
		psImage.setOrderNo(rs.getLong("order_no"));
		psImage.setFilename(rs.getString("filename"));
		psImage.setTitle(rs.getString("title"));
		psImage.setListingsId(rs.getLong("listing_id"));
		psImage.setDateadded(rs.getLong("dateadded"));
		psImage.setWatermark(rs.getInt("watermark"));
		psImage.setPhoto(rs.getInt("photo"));
		psImage.setTypeImage(rs.getString("type_image"));		
		return psImage;
	}
	

}
