/**
 * @File: EntityRefMapper.java
 */
package com.deals.migration.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.deals.migration.domain.EntityRef;
import com.deals.migration.domain.EntityRef.EntityType;

/**
 * @author shahinkonadath
 * @date 26-Apr-2015
 * @package com.deals.migration.mapper
 * @project deals-migration
 * @desc 
 */
public class EntityRefMapper implements RowMapper<EntityRef>{

	
	@Override
	public EntityRef mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		EntityRef entityRef = new EntityRef();
		entityRef.setId(rs.getInt("id"));
		entityRef.setClientId(rs.getLong("client_id"));
		entityRef.setClientCode(rs.getString("client_code"));
		entityRef.setEntity(EntityType.valueOf(rs.getString("entity")));
		entityRef.setEntityCode(rs.getString("entity_code"));
		entityRef.setRefNumber(rs.getLong("ref_number"));
		
		return entityRef;
	}

}
