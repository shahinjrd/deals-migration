/**
 * @File: RateCodeMapper.java
 */
package com.deals.migration.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.deals.migration.domain.RatesCode;

/**
 * @author shahinkonadath
 * @date 25-Apr-2015
 * @package com.deals.migration.mapper
 * @project deals-migration
 * @desc
 */
public class RatesCodeMapper implements RowMapper<RatesCode> {

	@Override
	public RatesCode mapRow(ResultSet rs, int rowNum) throws SQLException {
		RatesCode ratesCode = new RatesCode();
		ratesCode.setId(rs.getInt("id"));
		ratesCode.setCountry(rs.getString("country"));
		ratesCode.setDialingCode(rs.getString("dialing_code"));
		ratesCode.setIsActive(rs.getInt("is_active"));
		ratesCode.setRate(rs.getFloat("rate"));
		return ratesCode;
	}

}
