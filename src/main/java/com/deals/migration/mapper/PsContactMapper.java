/**
 * @File: PsContactMapper.java
 */
package com.deals.migration.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.deals.migration.domain.PsContact;

/**
 * @author shahinkonadath
 * @date 23-Apr-2015
 * @package com.deals.migration.mapper
 * @project deals-migration
 * @desc
 */
public class PsContactMapper implements RowMapper<PsContact> {

	@Override
	public PsContact mapRow(ResultSet rs, int rowNum) throws SQLException {

		PsContact psContact = new PsContact();
		psContact.setId(rs.getLong("id"));
		psContact.setRef(rs.getString("ref"));
		psContact.setSalutation(rs.getString("salutation"));
		psContact.setTitle(rs.getString("title"));
		psContact.setName(rs.getString("name"));
		psContact.setLastName(rs.getString("last_name"));
		psContact.setClientId(rs.getLong("client_id"));
		psContact.setUserId(rs.getLong("user_id"));
		psContact.setAssignedToId(rs.getLong("assigned_to_id"));
		psContact.setPhone(rs.getString("phone"));
		psContact.setMobile(rs.getString("mobile"));
		psContact.setMobile1(rs.getString("mobile_1"));
		psContact.setMobile2(rs.getLong("mobile_2"));
		psContact.setCCodePhone1(rs.getLong("c_code_phone_1"));
		psContact.setCCodePhone1P(rs.getLong("c_code_phone_1_p"));
		psContact.setMobileNoNew(rs.getString("mobile_no_new"));
		psContact.setMobileNoNewCcode(rs.getInt("mobile_no_new_ccode"));
		psContact.setMobileNoNewCcode1(rs.getLong("mobile_no_new_ccode_1"));
		psContact.setMobileNoNewCcode2(rs.getLong("mobile_no_new_ccode_2"));
		psContact.setEmail(rs.getString("email"));
		psContact.setDateadded(rs.getString("dateadded"));
		psContact.setDateupdated(rs.getLong("dateupdated"));
		psContact.setType(rs.getLong("type"));
		psContact.setDob(rs.getString("dob"));

		return psContact;
	}

}// End of the class