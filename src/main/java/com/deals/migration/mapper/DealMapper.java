/**
 * @File: DealMapper.java
 */
package com.deals.migration.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.deals.migration.domain.Deal;

/**
 * @author shahinkonadath
 * @date 22-Apr-2015
 * @package com.deals.migration.mapper
 * @project deals-migration
 * @desc
 */
public class DealMapper implements RowMapper<Deal> {

	@Override
	public Deal mapRow(ResultSet rs, int rowNum) throws SQLException {

		Deal deal = new Deal();
		deal.setId(rs.getLong("id"));
		deal.setClientId(rs.getLong("client_id"));
		deal.setUserId(rs.getLong("user_id"));
		deal.setRef(rs.getString("ref"));
		deal.setType(rs.getString("type"));
		deal.setLandlordSellerId(rs.getLong("landlord_seller_id"));
		deal.setTenantBuyerId(rs.getLong("tenant_buyer_id"));
		deal.setListingsId(rs.getLong("listings_id"));
		deal.setDealDate(rs.getLong("deal_date"));
		deal.setDeposit(rs.getLong("deposit"));
		deal.setCommission(rs.getString("commission"));
		deal.setRenewalDate(rs.getLong("renewal_date"));
		deal.setRemindBefore(rs.getLong("remind_before"));
		deal.setDocuments(rs.getString("documents"));
		deal.setNotes(rs.getString("notes"));

		return deal;

	}

}
