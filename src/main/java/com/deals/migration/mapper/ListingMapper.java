/**
 * @File: ListingMapper.java
 */
package com.deals.migration.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.deals.migration.domain.Listing;

/**
 * @author shahinkonadath
 * @date 23-Apr-2015
 * @package com.deals.migration.mapper
 * @project deals-migration
 * @desc 
 */
public class ListingMapper implements RowMapper<Listing>{

	
	@Override
	public Listing mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Listing listing = new Listing();
		listing.setId(rs.getLong("id"));
		listing.setClientId(rs.getLong("client_id"));
		listing.setUserId(rs.getLong("user_id"));
		listing.setRef(rs.getString("ref"));
		listing.setStreetNo(rs.getString("street_no"));
		listing.setCategoryId(rs.getLong("category_id"));
		listing.setBeds(rs.getString("beds"));
		listing.setBaths(rs.getString("baths"));
		listing.setSize(rs.getBigDecimal("size"));
		listing.setPlotSize(rs.getLong("plot_size"));
		listing.setPrice(rs.getLong("price"));
		listing.setCommission(rs.getLong("commission"));
		listing.setDescription(rs.getString("description"));
		listing.setFurnished(rs.getInt("furnished"));
		listing.setDateadded(rs.getLong("dateadded"));
		listing.setDateupdated(rs.getLong("dateupdated"));
		listing.setLandlordId(rs.getLong("landlord_id"));
		listing.setRegionId(rs.getLong("region_id"));
		listing.setAreaLocationId(rs.getLong("area_location_id"));
		listing.setSubAreaLocationId(rs.getLong("sub_area_location_id"));
		listing.setViewId(rs.getString("view_id"));
		listing.setPhotos(rs.getLong("photos"));
		listing.setFloorNo(rs.getInt("floor_no"));
		listing.setType(rs.getLong("type"));
		listing.setFitted(rs.getShort("fitted"));
		listing.setFeaturesId(rs.getString("features_id"));
		listing.setAvailableDate(rs.getLong("available_date"));
		listing.setUnitType(rs.getString("unit_type"));
		listing.setUnit(rs.getString("unit"));
		
		return listing;
	}

}
