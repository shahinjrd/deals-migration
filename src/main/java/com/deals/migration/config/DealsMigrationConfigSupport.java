/**
 * @File: DealsMigrationConfigSupport.java
 */
package com.deals.migration.config;

/**
 * @author shahinkonadath
 * @date 25-Apr-2015
 * @package com.deals.migration.config
 * @project deals-migration
 * @desc
 */
public class DealsMigrationConfigSupport {

	public enum Schema {
		PROPSPACE, PROPMGMT
	}

	public static final String STARTER_SQL = "SELECT * FROM cms_deals";

	public static String getDsName(Schema schema) {
		switch (schema) {
			case PROPSPACE: return "ps.";
			case PROPMGMT: return "pm.";
			default:
				return "";
		}
	}

}
