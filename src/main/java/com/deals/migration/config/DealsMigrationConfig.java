/**
 * @File: DealsMigrationConfig.java
 */
package com.deals.migration.config;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.transaction.PlatformTransactionManager;

import com.deals.migration.config.DealsMigrationConfigSupport.Schema;
import com.deals.migration.domain.CompositeUnit;
import com.deals.migration.domain.Deal;
import com.deals.migration.mapper.DealMapper;
import com.deals.migration.processor.DealsProcessor;
import com.deals.migration.reader.DealsReader;
import com.deals.migration.writer.CompositeUnitWriter;
import com.zaxxer.hikari.HikariDataSource;

/**
 * @author shahinkonadath
 * @date 21-Apr-2015
 * @package com.deals.migration.config
 * @project deals-migration
 * @desc
 */
@Configuration
@EnableBatchProcessing
@PropertySource("classpath:DATA-INF/datasource.properties")
public class DealsMigrationConfig {

	@Autowired
	private Environment env;

	@Bean
	public Step migrationStep() throws Exception {
		
		DealsReader dealsReader = new DealsReader(propspaceJdbcTemplate(), new DealMapper());
		dealsReader.setSql(DealsMigrationConfigSupport.STARTER_SQL);		
		dealsReader.setFetchSize(20);//For reading records in a batch

		StepBuilderFactory stepBuilderFactory = new StepBuilderFactory(jobRepository(), transactionManager());
		return stepBuilderFactory.get("migrationStep").<Deal, CompositeUnit> chunk(10).reader(dealsReader)
				.processor(new DealsProcessor(propspaceJdbcTemplate())).writer(new CompositeUnitWriter(propmgmtJdbcTemplate())).build();
	}

	@Bean
	public Job migrateDeals() throws Exception {
		JobBuilderFactory jobBuilderFactory =  new JobBuilderFactory(jobRepository());
		return jobBuilderFactory.get("migrateDeals").incrementer(new RunIdIncrementer()).flow(migrationStep()).end().build();
	}

	@Bean
	public JdbcTemplate propspaceJdbcTemplate() {
		return new JdbcTemplate(getDataSource(Schema.PROPSPACE));
	}
	
	@Bean
	public JdbcTemplate propmgmtJdbcTemplate() {
		return new JdbcTemplate(getDataSource(Schema.PROPMGMT));
	}

	@Bean
	public JdbcTemplate jdbcHsqlTemplate() {
		return new JdbcTemplate(hsqlDataSource());
	}

	@Bean
	public JobRepository jobRepository() throws Exception {
		return new MapJobRepositoryFactoryBean(transactionManager()).getObject();
	}

	@Bean
	public JobLauncher jobLauncher() throws Exception {
		SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
		jobLauncher.setJobRepository(jobRepository());
		return jobLauncher;
	}
	

	private DataSource getDataSource(Schema schema) {
		String dsName = DealsMigrationConfigSupport.getDsName(schema);
		HikariDataSource dataSource = new HikariDataSource();
		// dataSource.setDriverClassName(env.getProperty("mysql.database.driver"));
		dataSource.setDriverClassName(env.getProperty("mysql.database.driver"));
		dataSource.setJdbcUrl(env.getProperty(dsName+"database.url"));
		dataSource.setUsername(env.getProperty(dsName+"database.username"));
		dataSource.setPassword(env.getProperty(dsName+"database.password"));
		dataSource.setAutoCommit(Boolean.valueOf(env.getProperty(dsName+"database.auto.commit")));
		dataSource.setMinimumIdle(Integer.valueOf(env.getProperty(dsName+"database.min.idle")));
		dataSource.setMaximumPoolSize(Integer.valueOf(env.getProperty(dsName+"database.max.pool.size")));
		dataSource.setConnectionTimeout(55000L);
		dataSource.setAutoCommit(true);
		dataSource.setReadOnly(false);
		return dataSource;
	}
	
	private PlatformTransactionManager transactionManager() {
		return new DataSourceTransactionManager(hsqlDataSource());
	}

	private DataSource hsqlDataSource() {
		EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
		return builder.setType(EmbeddedDatabaseType.HSQL).build();
	}

}