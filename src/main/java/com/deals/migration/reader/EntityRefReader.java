/**
 * @File: EntityRefReader.java
 */
package com.deals.migration.reader;

import org.springframework.jdbc.core.JdbcTemplate;

import com.deals.migration.domain.EntityRef;
import com.deals.migration.domain.EntityRef.EntityType;
import com.deals.migration.mapper.EntityRefMapper;

/**
 * @author shahinkonadath
 * @date 26-Apr-2015
 * @package com.deals.migration.reader
 * @project deals-migration
 * @desc
 */
public class EntityRefReader {

	private JdbcTemplate jdbcTemplate;

	/**
	 * @Constructor EntityRefReader
	 * @param jdbcTemplate
	 */
	public EntityRefReader(JdbcTemplate jdbcTemplate) {
		super();
		this.jdbcTemplate = jdbcTemplate;
	}

	public String getRefNumber(Long clientId, EntityType entityType) throws Exception {
		
		String sql = "SELECT * FROM cms_entity_ref WHERE client_id=? AND entity=?";
		EntityRef entityRef = jdbcTemplate.queryForObject(sql, new Object[] { clientId, entityType.name() },
				new EntityRefMapper());
		Long refNumber = entityRef.getRefNumber() + 1;

		String updateSql = "UPDATE cms_entity_ref SET ref_number=" + refNumber + " WHERE client_id=" + entityRef.getClientId()
				+ " AND entity='" + entityRef.getEntity().name() + "'";
		int rowsUpdated = jdbcTemplate.update(updateSql);
		if (rowsUpdated != 1) {
			throw new Exception("Can't continue as the application failed to update the entity reference number for: "
					+ entityType.name());
		}
		return entityRef.getClientCode() + "-" + entityRef.getEntityCode() + "-" + refNumber;
	}
}
