/**
 * @File: PsImagesReader.java
 */
package com.deals.migration.reader;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.Assert;

import com.deals.migration.domain.PsImage;
import com.deals.migration.mapper.PsImagesMapper;

/**
 * @author shahinkonadath
 * @date 25-Apr-2015
 * @package com.deals.migration.reader
 * @project deals-migration
 * @desc
 */
public class PsImagesReader {

	private JdbcTemplate jdbcTemplate;

	/**
	 * @Constructor PsImagesReader
	 * @param jdbcTemplate
	 */
	public PsImagesReader(DataSource dataSource) {
		Assert.notNull(dataSource, "A datasorce should be supplied");
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * @Constructor PsImagesReader
	 * @param jdbcTemplate
	 */
	public PsImagesReader(JdbcTemplate jdbcTemplate) {
		Assert.notNull(jdbcTemplate, "A JdbcTemplate should be supplied");
		this.jdbcTemplate = jdbcTemplate;
	}

	public PsImage findImage(Long id) {
		String sql = "SELECT * FROM cms_images WHERE id=?";
		return jdbcTemplate.queryForObject(sql, new Object[] { id }, new PsImagesMapper());
	}

}// End of the class