/**
 * @File: ListingReader.java
 */
package com.deals.migration.reader;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.Assert;

import com.deals.migration.domain.Listing;
import com.deals.migration.mapper.ListingMapper;

/**
 * @author shahinkonadath
 * @date 23-Apr-2015
 * @package com.deals.migration.reader
 * @project deals-migration
 * @desc
 */
public class ListingReader {

	private JdbcTemplate jdbcTemplate;

	/**
	 * @Constructor ListingReader
	 */
	public ListingReader(DataSource dataSource) {
		Assert.notNull(dataSource, "A datasorce should be supplied");
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * @Constructor ListingReader
	 * @param jdbcTemplate
	 */
	public ListingReader(JdbcTemplate jdbcTemplate) {
		Assert.notNull(jdbcTemplate, "A JdbcTemplate should be supplied");
		this.jdbcTemplate = jdbcTemplate;
	}

	public Listing findListing(Long id) {
		String sql = "SELECT * FROM cms_listings WHERE id=?";
		return jdbcTemplate.queryForObject(sql, new Object[] { id }, new ListingMapper());
	}

}// End of the class