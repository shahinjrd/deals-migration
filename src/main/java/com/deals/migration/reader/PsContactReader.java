/**
 * @File: PsContactReader.java
 */
package com.deals.migration.reader;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.Assert;

import com.deals.migration.domain.PsContact;
import com.deals.migration.mapper.PsContactMapper;

/**
 * @author shahinkonadath
 * @date 23-Apr-2015
 * @package com.deals.migration.reader
 * @project deals-migration
 * @desc
 */
public class PsContactReader {

	private JdbcTemplate jdbcTemplate;

	/**
	 * @Constructor PsContactReader
	 * @param jdbcTemplate
	 */
	public PsContactReader(DataSource dataSource) {
		Assert.notNull(dataSource, "A datasorce should be supplied");
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * @Constructor PsContactReader
	 * @param jdbcTemplate
	 */
	public PsContactReader(JdbcTemplate jdbcTemplate) {
		Assert.notNull(jdbcTemplate, "A JdbcTemplate should be supplied");
		this.jdbcTemplate = jdbcTemplate;
	}

	public PsContact findPsContact(Long id) {
		String sql = "SELECT * FROM cms_landlord WHERE id=?";
		return jdbcTemplate.queryForObject(sql, new Object[] { id }, new PsContactMapper());
	}

}
