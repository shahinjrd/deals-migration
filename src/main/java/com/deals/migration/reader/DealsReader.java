/**
 * @File: DealsReader.java
 */
package com.deals.migration.reader;

import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.deals.migration.domain.Deal;

/**
 * @author shahinkonadath
 * @date 22-Apr-2015
 * @package com.deals.migration.reader
 * @project deals-migration
 * @desc
 */
public class DealsReader extends JdbcCursorItemReader<Deal> {

	/**
	 * @Constructor DealsReader
	 */
	public DealsReader(JdbcTemplate jdbcTemplate, RowMapper<Deal> dealMapper) {
		super();
		super.setDataSource(jdbcTemplate.getDataSource());
		super.setRowMapper(dealMapper);
	}
	
	

}
