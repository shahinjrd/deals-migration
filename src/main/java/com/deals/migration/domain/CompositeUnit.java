/**
 * @File: CompositeUnit.java
 */
package com.deals.migration.domain;

/**
 * @author shahinkonadath
 * @date 25-Apr-2015
 * @package com.deals.migration.domain
 * @project deals-migration
 * @desc
 */
public class CompositeUnit {

	private Unit unit;
	private PmContact landlord;
	private PmContact tenant;

	/**
	 * @Constructor CompositeUnit
	 */
	public CompositeUnit() {
	}

	/**
	 * @Constructor CompositeUnit
	 * @param unit
	 * @param landlord
	 * @param tenant
	 */
	public CompositeUnit(Unit unit, PmContact landlord, PmContact tenant) {
		this.unit = unit;
		this.landlord = landlord;
		this.tenant = tenant;
	}

	/**
	 * @return the unit
	 */
	public Unit getUnit() {
		return unit;
	}

	/**
	 * @param unit
	 *            the unit to set
	 */
	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	/**
	 * @return the landlord
	 */
	public PmContact getLandlord() {
		return landlord;
	}

	/**
	 * @param landlord
	 *            the landlord to set
	 */
	public void setLandlord(PmContact landlord) {
		this.landlord = landlord;
	}

	/**
	 * @return the tenant
	 */
	public PmContact getTenant() {
		return tenant;
	}

	/**
	 * @param tenant
	 *            the tenant to set
	 */
	public void setTenant(PmContact tenant) {
		this.tenant = tenant;
	}

	
	@Override
	public String toString() {
		return "CompositeUnit [unit=" + unit + ", landlord=" + landlord + ", tenant=" + tenant + "]";
	}	
	

}// End of the class