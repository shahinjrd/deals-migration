/**
 * @File: EntityRef.java
 */
package com.deals.migration.domain;

import java.io.Serializable;

/**
 * @author shahinkonadath
 * @date 26-Apr-2015
 * @package com.deals.migration.domain
 * @project deals-migration
 * @desc
 */
public class EntityRef implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -5630092822258608192L;

	public enum EntityType {
		Unit, Lease, Listing, History, WorkOrder, Contact, ServiceProvider, Account
	}

	private Integer id;
	private Long clientId;
	private String clientCode;
	private EntityType entity;
	private String entityCode;
	private Long refNumber;

	public EntityRef() {
		super();
	}

	public EntityRef(Integer id, Long clientId, String clientCode, EntityType entity, Long refNumber) {
		super();
		this.id = id;
		this.clientId = clientId;
		this.clientCode = clientCode;
		this.entity = entity;
		this.refNumber = refNumber;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the clientId
	 */
	public Long getClientId() {
		return clientId;
	}

	/**
	 * @param clientId
	 *            the clientId to set
	 */
	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	/**
	 * @return the clientCode
	 */
	public String getClientCode() {
		return clientCode;
	}

	/**
	 * @param clientCode
	 *            the clientCode to set
	 */
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	/**
	 * @return the entity
	 */
	public EntityType getEntity() {
		return entity;
	}

	/**
	 * @param entity
	 *            the entity to set
	 */
	public void setEntity(EntityType entity) {
		this.entity = entity;
	}

	/**
	 * @return the entityCode
	 */
	public String getEntityCode() {
		return entityCode;
	}

	/**
	 * @param entityCode
	 *            the entityCode to set
	 */
	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	/**
	 * @return the refNumber
	 */
	public Long getRefNumber() {
		return refNumber;
	}

	/**
	 * @param refNumber
	 *            the refNumber to set
	 */
	public void setRefNumber(Long refNumber) {
		this.refNumber = refNumber;
	}

	@Override
	public String toString() {
		return "EntityRef [id=" + id + ", clientId=" + clientId + ", clientCode=" + clientCode + ", entity=" + entity
				+ ", entityCode=" + entityCode + ", refNumber=" + refNumber + "]";
	}

}// End of the class