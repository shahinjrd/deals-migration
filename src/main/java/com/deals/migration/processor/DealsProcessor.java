/**
 * @File: DealsProcessor.java
 */
package com.deals.migration.processor;

import java.util.Date;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.Assert;

import com.deals.migration.domain.CompositeUnit;
import com.deals.migration.domain.Deal;
import com.deals.migration.domain.Listing;
import com.deals.migration.domain.PmContact;
import com.deals.migration.domain.PsContact;
import com.deals.migration.domain.Unit;
import com.deals.migration.domain.EntityRef.EntityType;
import com.deals.migration.reader.EntityRefReader;
import com.deals.migration.reader.ListingReader;
import com.deals.migration.reader.PsContactReader;

/**
 * @author shahinkonadath
 * @date 22-Apr-2015
 * @package com.deals.migration.processor
 * @project deals-migration
 * @desc
 */
public class DealsProcessor implements ItemProcessor<Deal, CompositeUnit> {

	private ContactProcessor contactProcessor = new ContactProcessor();
	private ListingReader listingReader;
	private PsContactReader psContactReader;
	private EntityRefReader entityRefReader;	
	
	
	private final static String FURNISHED = "yes";
	private final static String NOT_FURNISHED = "no";
	private final static String PARTLY_FURNISHED = "partially";
	private final static String NOT_SPECIFIED = null;

	/**
	 * @Constructor DealsProcessor
	 */
	public DealsProcessor(JdbcTemplate propspaceJdbcTemplate) {
		this.listingReader = new ListingReader(propspaceJdbcTemplate);
		this.psContactReader = new PsContactReader(propspaceJdbcTemplate);
		this.entityRefReader = new EntityRefReader(propspaceJdbcTemplate);
	}

	@Override
	public CompositeUnit process(Deal deal) throws Exception {

		DealExtractHolder dealExtractHolder = validateAndExtractDeal(deal);

		if (dealExtractHolder.isValidated) {

			System.out.println("Valid deal: " + deal.toString());
			Listing listing = dealExtractHolder.getListing();
			
			Unit unit = new Unit();
			unit.setClientId(listing.getClientId());
			unit.setUserId(listing.getUserId());
			//unit.setRefCode(listing.getRef());
			unit.setRefCode(entityRefReader.getRefNumber(unit.getClientId(), EntityType.Unit));
			unit.setBaths(listing.getBaths());
			unit.setBeds(listing.getBeds());
			unit.setFitted(getFitted(listing.getFitted()));
			unit.setCategoryId(Integer.valueOf(String.valueOf(listing.getCategoryId())));
			unit.setFees(Double.valueOf((null == deal.getCommission() || deal.getCommission().equals("")) ? "0.0" : deal
					.getCommission()));
			unit.setFurnished(getFurnished(listing.getFurnished()));
			unit.setBuiltUpArea(Float.valueOf(String.valueOf(listing.getSize())));
			unit.setGuidePrice(Double.valueOf(String.valueOf(listing.getPrice())));
			unit.setDateAdded(new Date(listing.getDateadded()));
			unit.setDateUpdated(new Date(listing.getDateupdated()));
			unit.setLandlordId(dealExtractHolder.getLandlord().getId());
			unit.setRegionId(listing.getRegionId());
			unit.setLocationId(listing.getAreaLocationId());
			unit.setSubLocationId(listing.getSubAreaLocationId());
			unit.setType(listing.getUnitType());
			unit.setView(listing.getViewId());
			unit.setImages(listing.getFeaturesId());
			unit.setNextAvlDate(new Date(listing.getAvailableDate()));
			unit.setUnitNumber(listing.getUnit());
			
			System.out.println(unit.toString());

			PmContact landlord = contactProcessor.process(dealExtractHolder.getLandlord());
			PmContact tenant = contactProcessor.process(dealExtractHolder.getTenant());

			return new CompositeUnit(unit, landlord, tenant);

		} else {
			// System.out.println("Skipped deal:" + deal.toString());
			return null;
		}

	}

	/**
	 * @return
	 */
	private DealExtractHolder validateAndExtractDeal(Deal deal) {

		try {

			Listing listing = validateListing(deal.getListingsId());
			Assert.notNull(listing, "The deal doesn't have a valid listing");
			// System.out.println(listing.toString());

			PsContact landlord = validateLandlord(listing.getLandlordId(), deal.getLandlordSellerId());
			Assert.notNull(landlord, "The deal doesn't have a valid landlord");
			// System.out.println("Landlord: " + landlord.toString());

			PsContact tenant = validateTenant(deal.getTenantBuyerId());
			Assert.notNull(tenant, "The deal doesn't have a valid tenant");
			// System.out.println("Tenant: " + tenant.toString());

			return new DealExtractHolder(true, listing, landlord, tenant);

		} catch (Exception e) {
			// System.out.println(e.toString());
			// e.printStackTrace();
			return new DealExtractHolder(false, null, null, null);
		}
	}

	/**
	 * @param tenantBuyerId
	 * @return
	 */
	private PsContact validateTenant(Long tenantBuyerId) {

		if (null != tenantBuyerId && tenantBuyerId != 0) {
			return psContactReader.findPsContact(tenantBuyerId);
		} else {
			return null;
		}

	}

	/**
	 * @param landlordId
	 * @param landlordSellerId
	 * @return
	 */
	private PsContact validateLandlord(Long landlordId, Long landlordSellerId) {

		if (null != landlordSellerId && landlordSellerId != 0) {
			return psContactReader.findPsContact(landlordSellerId);
		} else if (null != landlordId && landlordId != 0) {
			return psContactReader.findPsContact(landlordId);
		} else {
			return null;
		}

	}

	/**
	 * @param listingsId
	 * @return
	 */
	private Listing validateListing(Long listingsId) {

		if (null != listingsId && listingsId != 0) {
			return listingReader.findListing(listingsId);
		} else {
			return null;
		}

	}

	/**
	 * 
	 * @param fitted
	 * @return
	 */
	private String getFitted(short fitted) {
		switch (fitted) {
			case 1:
				return "fittedSpace";
			case 2:
				return "notFitted";
			case 3:
				return "semiFitted";
			case 4:
				return "shellAndCore";
			default:
				return "";
		}
	}

	/**
	 * @param furnished
	 * @return
	 */
	private String getFurnished(int furnished) {
		switch (furnished) {
			case 1:
				return FURNISHED;
			case 2:
				return NOT_FURNISHED;
			case 3:
				return PARTLY_FURNISHED;
			default:
				return NOT_SPECIFIED;
		}
	}

	@SuppressWarnings("unused")
	private static class DealExtractHolder {

		private Boolean isValidated;
		private Listing listing;
		private PsContact landlord;
		private PsContact tenant;

		/**
		 * @Constructor DealExtractHolder
		 * @param isValidated
		 * @param listing
		 * @param landlord
		 * @param tenant
		 */
		public DealExtractHolder(Boolean isValidated, Listing listing, PsContact landlord, PsContact tenant) {
			this.isValidated = isValidated;
			this.listing = listing;
			this.landlord = landlord;
			this.tenant = tenant;
		}

		/**
		 * @return the isValidated
		 */
		public Boolean getIsValidated() {
			return isValidated;
		}

		/**
		 * @param isValidated
		 *            the isValidated to set
		 */
		public void setIsValidated(Boolean isValidated) {
			this.isValidated = isValidated;
		}

		/**
		 * @return the listing
		 */
		public Listing getListing() {
			return listing;
		}

		/**
		 * @param listing
		 *            the listing to set
		 */
		public void setListing(Listing listing) {
			this.listing = listing;
		}

		/**
		 * @return the landlord
		 */
		public PsContact getLandlord() {
			return landlord;
		}

		/**
		 * @param landlord
		 *            the landlord to set
		 */
		public void setLandlord(PsContact landlord) {
			this.landlord = landlord;
		}

		/**
		 * @return the tenant
		 */
		public PsContact getTenant() {
			return tenant;
		}

		/**
		 * @param tenant
		 *            the tenant to set
		 */
		public void setTenant(PsContact tenant) {
			this.tenant = tenant;
		}

	}// End of static class

}// End of the class