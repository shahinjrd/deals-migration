/**
 * @File: ContactProcessor.java
 */
package com.deals.migration.processor;

import org.springframework.batch.item.ItemProcessor;

import com.deals.migration.domain.PmContact;
import com.deals.migration.domain.PsContact;

/**
 * @author shahinkonadath
 * @date 23-Apr-2015
 * @package com.deals.migration.processor
 * @project deals-migration
 * @desc 
 */
public class ContactProcessor implements ItemProcessor<PsContact, PmContact>{

	@Override
	public PmContact process(PsContact psContact) throws Exception {
		PmContact pmContact = new PmContact(); 
		pmContact.setClientId(psContact.getClientId());
		pmContact.setUserId(psContact.getUserId());
		return pmContact;
	}
	

}
