/**
 * @File: PmContactWriter.java
 */
package com.deals.migration.writer;

import org.springframework.batch.item.database.JdbcBatchItemWriter;

import com.deals.migration.domain.PmContact;

/**
 * @author shahinkonadath
 * @date 23-Apr-2015
 * @package com.deals.migration.writer
 * @project deals-migration
 * @desc 
 */
public class PmContactWriter extends JdbcBatchItemWriter<PmContact>{

}
