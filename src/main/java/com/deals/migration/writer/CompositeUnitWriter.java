/**
 * @File: CompositeUnitWriter.java
 */
package com.deals.migration.writer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.deals.migration.domain.CompositeUnit;
import com.deals.migration.domain.PmContact;
import com.deals.migration.domain.Unit;
import com.deals.migration.statements.PmContactStatement;
import com.deals.migration.statements.UnitStatement;

/**
 * @author shahinkonadath
 * @date 22-Apr-2015
 * @package com.deals.migration.processor
 * @project deals-migration
 * @desc The composite writer for unit and its dependencies
 */
public class CompositeUnitWriter implements ItemWriter<CompositeUnit> {

	private List<PmContact> contactList = new ArrayList<PmContact>(0);
	private List<Unit> unitList = new ArrayList<Unit>(0);
	private JdbcTemplate propmgmtJdbcTemplate;

	/**
	 * @Constructor CompositeUnitWriter
	 * @param propmgmtJdbcTemplate
	 */
	public CompositeUnitWriter(JdbcTemplate propmgmtJdbcTemplate) {
		super();
		this.propmgmtJdbcTemplate = propmgmtJdbcTemplate;
	}

	@Override
	public void write(List<? extends CompositeUnit> compositeUnits) throws Exception {

		for (CompositeUnit compositeUnit : compositeUnits) {
			if (null != compositeUnit) {
				contactList.add(compositeUnit.getLandlord());
				contactList.add(compositeUnit.getTenant());
				unitList.add(compositeUnit.getUnit());
			}
		}

		writeData();

	}

	/**
	 * @param contactList
	 * @param unitList
	 */
	private void writeData() {
		try {
			
			getUnitWriter().write(unitList);
			getContactWriter().write(contactList);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			contactList.clear();
			unitList.clear();
		}

	}

	/**
	 * @return
	 */
	private UnitWriter getUnitWriter() {
		UnitWriter unitWriter = new UnitWriter();
		UnitStatement unitStatement = new UnitStatement();
		unitWriter.setJdbcTemplate(new NamedParameterJdbcTemplate(propmgmtJdbcTemplate));
		unitWriter.setItemPreparedStatementSetter(unitStatement);
		unitWriter.setSql(unitStatement.getSql());
		unitWriter.afterPropertiesSet();
		return unitWriter;
	}

	/**
	 * @return
	 */
	private PmContactWriter getContactWriter() {
		PmContactWriter pmContactWriter = new PmContactWriter();
		PmContactStatement pmContactStatement = new PmContactStatement();
		pmContactWriter.setJdbcTemplate(new NamedParameterJdbcTemplate(propmgmtJdbcTemplate));
		pmContactWriter.setItemPreparedStatementSetter(pmContactStatement);
		pmContactWriter.setSql(pmContactStatement.getSql());
		pmContactWriter.afterPropertiesSet();
		return pmContactWriter;
	}

}// End of the class