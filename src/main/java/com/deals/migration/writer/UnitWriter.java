/**
 * @File: UnitWriter.java
 */
package com.deals.migration.writer;

import org.springframework.batch.item.database.JdbcBatchItemWriter;

import com.deals.migration.domain.Unit;

/**
 * @author shahinkonadath
 * @date 25-Apr-2015
 * @package com.deals.migration.writer
 * @project deals-migration
 * @desc 
 */
public class UnitWriter extends JdbcBatchItemWriter<Unit>{

}
