/**
 * @File: PropspaceLogger.java
 */
package com.deals.migration.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author shahinkonadath
 * @date 18-Mar-2015
 * @package com.propspace.intl.util.logging
 * @project propspace-utils
 * @desc 
 */
public class DmLogger {
	
	private Logger logger;

	/**
	 * Constructor
	 */
	public DmLogger(Class<?> loggedClass) {
		this.logger = LoggerFactory.getLogger(loggedClass.getSimpleName());
	}

	/**
	 * @param string
	 */
	public void debug(String msg) {
		logger.debug(msg);
	}
	
	/**
	 * @param string
	 */
	public void info(String msg) {
		logger.info(msg);
	}
	
	/**
	 * @param string
	 */
	public void error(String msg) {
		logger.error(msg);
	}
	
	/**
	 * @param string
	 */
	public void warn(String msg) {
		logger.warn(msg);
	}
	

}//End of the class